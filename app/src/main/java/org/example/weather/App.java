package org.example.weather;

import android.app.Application;

import org.example.weather.di.component.AppComponent;
import org.example.weather.di.component.DaggerAppComponent;
import org.example.weather.di.module.AppModule;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
public class App extends Application {

    private AppComponent appComponent;

    public AppComponent component() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

}
