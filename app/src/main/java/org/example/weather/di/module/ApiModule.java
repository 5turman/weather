package org.example.weather.di.module;

import org.example.weather.api.WeatherApi;
import org.example.weather.di.scope.PerApp;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.SimpleXmlConverterFactory;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
@Module
public class ApiModule {

    private static final String BASE_URL = "http://informer.gismeteo.ru/";

    @Provides
    @PerApp
    public Retrofit provideRetrofit() {

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
    }

    @Provides
    @PerApp
    public WeatherApi providerWeatherApi(Retrofit retrofit) {
        return retrofit.create(WeatherApi.class);
    }

}
