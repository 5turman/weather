package org.example.weather.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerApp {
}
