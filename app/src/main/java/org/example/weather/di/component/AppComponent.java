package org.example.weather.di.component;

import org.example.weather.di.module.ApiModule;
import org.example.weather.di.module.AppModule;
import org.example.weather.di.scope.PerApp;
import org.example.weather.ui.fragment.ForecastListFragment;

import dagger.Component;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
@PerApp
@Component(modules = {
        AppModule.class,
        ApiModule.class
})
public interface AppComponent {

    void inject(ForecastListFragment fragment);

}
