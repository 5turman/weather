package org.example.weather.di.module;

import android.content.Context;
import android.content.res.Resources;

import org.example.weather.App;
import org.example.weather.di.qualifier.AppContext;
import org.example.weather.di.scope.PerApp;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
@Module
public class AppModule {

    private final App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @PerApp
    @AppContext
    public Context provideContext() {
        return app;
    }

    @Provides
    public Resources provideResources() {
        return app.getResources();
    }

}
