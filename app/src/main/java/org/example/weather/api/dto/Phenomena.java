package org.example.weather.api.dto;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Attribute;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
public class Phenomena implements Parcelable {

    @Attribute
    private int cloudiness;

    @Attribute
    private int precipitation;

    @Attribute
    private int rpower;

    @Attribute
    private int spower;

    public Phenomena() {
    }

    protected Phenomena(Parcel in) {
        cloudiness = in.readInt();
        precipitation = in.readInt();
        rpower = in.readInt();
        spower = in.readInt();
    }

    public static final Creator<Phenomena> CREATOR = new Creator<Phenomena>() {
        @Override
        public Phenomena createFromParcel(Parcel in) {
            return new Phenomena(in);
        }

        @Override
        public Phenomena[] newArray(int size) {
            return new Phenomena[size];
        }
    };

    public int getCloudiness() {
        return cloudiness;
    }

    public int getPrecipitation() {
        return precipitation;
    }

    public int getRpower() {
        return rpower;
    }

    public int getSpower() {
        return spower;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(cloudiness);
        dest.writeInt(precipitation);
        dest.writeInt(rpower);
        dest.writeInt(spower);
    }

}
