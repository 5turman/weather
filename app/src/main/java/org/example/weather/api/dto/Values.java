package org.example.weather.api.dto;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Attribute;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
public class Values implements Parcelable {

    @Attribute
    private int max;

    @Attribute
    private int min;

    public Values() {
    }

    protected Values(Parcel in) {
        max = in.readInt();
        min = in.readInt();
    }

    public static final Creator<Values> CREATOR = new Creator<Values>() {
        @Override
        public Values createFromParcel(Parcel in) {
            return new Values(in);
        }

        @Override
        public Values[] newArray(int size) {
            return new Values[size];
        }
    };

    public int getMax() {
        return max;
    }

    public int getMin() {
        return min;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(max);
        dest.writeInt(min);
    }

}
