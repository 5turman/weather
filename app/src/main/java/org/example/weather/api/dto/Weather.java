package org.example.weather.api.dto;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
@Root(strict = false)
public class Weather {

    @Path("REPORT[1]")
    @ElementList(name = "TOWN")
    private List<Forecast> forecasts;

    public List<Forecast> getForecasts() {
        return forecasts;
    }

}
