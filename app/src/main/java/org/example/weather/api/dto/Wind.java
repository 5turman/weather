package org.example.weather.api.dto;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Attribute;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
public class Wind extends Values implements Parcelable {

    @Attribute
    private int direction;

    public Wind() {
    }

    protected Wind(Parcel in) {
        super(in);
        direction = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(direction);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Wind> CREATOR = new Creator<Wind>() {
        @Override
        public Wind createFromParcel(Parcel in) {
            return new Wind(in);
        }

        @Override
        public Wind[] newArray(int size) {
            return new Wind[size];
        }
    };

    public int getDirection() {
        return direction;
    }

}
