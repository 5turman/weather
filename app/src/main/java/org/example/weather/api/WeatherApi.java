package org.example.weather.api;

import org.example.weather.api.dto.Weather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
public interface WeatherApi {

    @GET("xml/{id}_1.xml")
    Call<Weather> get(@Path("id") int cityId);

}
