package org.example.weather.api.dto;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
public class Forecast implements Parcelable {

    @Attribute
    private int day;

    @Attribute
    private int month;

    @Attribute
    private int year;

    @Attribute
    private int hour;

    @Attribute
    private int tod;

    @Attribute
    private int predict;

    @Attribute
    private int weekday;

    @Element(name = "PHENOMENA")
    private Phenomena phenomena;

    @Element(name = "PRESSURE")
    private Values pressure;

    @Element(name = "TEMPERATURE")
    private Values temperature;

    @Element(name = "WIND")
    private Wind wind;

    @Element(name = "RELWET")
    private Values relwet;

    @Element(name = "HEAT")
    private Values heat;

    public Forecast() {
    }

    protected Forecast(Parcel in) {
        day = in.readInt();
        month = in.readInt();
        year = in.readInt();
        hour = in.readInt();
        tod = in.readInt();
        predict = in.readInt();
        weekday = in.readInt();
        phenomena = in.readParcelable(Phenomena.class.getClassLoader());
        pressure = in.readParcelable(Values.class.getClassLoader());
        temperature = in.readParcelable(Values.class.getClassLoader());
        wind = in.readParcelable(Wind.class.getClassLoader());
        relwet = in.readParcelable(Values.class.getClassLoader());
        heat = in.readParcelable(Values.class.getClassLoader());
    }

    public static final Creator<Forecast> CREATOR = new Creator<Forecast>() {
        @Override
        public Forecast createFromParcel(Parcel in) {
            return new Forecast(in);
        }

        @Override
        public Forecast[] newArray(int size) {
            return new Forecast[size];
        }
    };

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public int getHour() {
        return hour;
    }

    public int getTod() {
        return tod;
    }

    public int getPredict() {
        return predict;
    }

    public int getWeekday() {
        return weekday;
    }

    public Phenomena getPhenomena() {
        return phenomena;
    }

    public Values getPressure() {
        return pressure;
    }

    public Values getTemperature() {
        return temperature;
    }

    public Wind getWind() {
        return wind;
    }

    public Values getRelwet() {
        return relwet;
    }

    public Values getHeat() {
        return heat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(day);
        dest.writeInt(month);
        dest.writeInt(year);
        dest.writeInt(hour);
        dest.writeInt(tod);
        dest.writeInt(predict);
        dest.writeInt(weekday);
        dest.writeParcelable(phenomena, flags);
        dest.writeParcelable(pressure, flags);
        dest.writeParcelable(temperature, flags);
        dest.writeParcelable(wind, flags);
        dest.writeParcelable(relwet, flags);
        dest.writeParcelable(heat, flags);
    }

}
