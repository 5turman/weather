package org.example.weather.util;

import java.util.List;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
public class CollectionUtils {

    public static <T> boolean isEmpty(List<T> list) {
        return (list == null) || list.isEmpty();
    }

}
