package org.example.weather.util;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.view.View;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
public class Views {

    @SuppressWarnings("unchecked")
    public static <T extends View> T find(View source, @IdRes int id) {
        return (T) source.findViewById(id);
    }

    @SuppressWarnings("unchecked")
    public static <T extends View> T find(Activity source, @IdRes int id) {
        return (T) source.findViewById(id);
    }

}
