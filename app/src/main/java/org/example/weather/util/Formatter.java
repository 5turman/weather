package org.example.weather.util;

import org.example.weather.api.dto.Forecast;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
public class Formatter {

    public static String formatDateTime(Forecast forecast) {
        // For best results we can use android.text.format.DateUtils#formatDateTime
        return String.format(
                "%02d.%02d.%d %02d:00",
                forecast.getDay(), forecast.getMonth(), forecast.getYear(), forecast.getHour()
        );
    }

    public static String formatTemp(int value) {
        return String.format("%+d°", value);
    }

}
