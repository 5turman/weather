package org.example.weather.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.example.weather.R;
import org.example.weather.api.WeatherApi;
import org.example.weather.api.dto.Forecast;
import org.example.weather.api.dto.Weather;
import org.example.weather.di.component.AppComponent;
import org.example.weather.ui.activity.ForecastActivity;
import org.example.weather.ui.adapter.ForecastAdapter;
import org.example.weather.ui.graphics.ListItemDecoration;
import org.example.weather.util.Views;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.example.weather.util.CollectionUtils.isEmpty;
import static org.example.weather.util.ConnectivityUtils.isConnected;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
public class ForecastListFragment extends BaseFragment {

    private static final int CITY_ID = 29634;

    @Inject
    protected WeatherApi mApi;

    private ForecastAdapter mAdapter;

    private TextView mZeroView;
    private View mProgressView;
    private View mUpdateButton;

    private boolean mLoading;
    private boolean mHasError;
    private Call<Weather> mWeatherCall;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        mAdapter = new ForecastAdapter(mAdapterCallback);
    }

    @Override
    protected void inject(AppComponent component) {
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final Context context = inflater.getContext();

        View view = inflater.inflate(R.layout.fragment_forecast_list, container, false);

        RecyclerView recyclerView = Views.find(view, R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(mAdapter);
        recyclerView.setHasFixedSize(true);

        recyclerView.addItemDecoration(
                new ListItemDecoration(context, false)
        );

        mZeroView = Views.find(view, R.id.zero_view);
        mProgressView = view.findViewById(R.id.progress);

        mUpdateButton = view.findViewById(R.id.button_update);
        mUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mLoading) {
            show(mProgressView);
        } else if (mHasError) {
            showError();
        } else if (mAdapter.getItemCount() == 0) {
            showNoData();
        }
    }

    @Override
    public void onDestroyView() {
        mZeroView = null;
        mProgressView = null;
        mUpdateButton = null;

        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mWeatherCall != null) {
            mWeatherCall.cancel();
        }
    }

    private void loadData() {

        if (isConnected(getContext())) {

            setLoading(true);

            mAdapter.setItems(null);

            if (mWeatherCall != null) {
                mWeatherCall.cancel();
            }

            mWeatherCall = mApi.get(CITY_ID);
            mWeatherCall.enqueue(mWeatherCallback);

        } else if (getView() != null) {

            Snackbar.make(getView(), R.string.error_no_connection, Snackbar.LENGTH_LONG).show();
        }
    }

    private void setLoading(boolean loading) {
        mLoading = loading;
        if (mProgressView != null) {
            mProgressView.setVisibility(loading ? View.VISIBLE : View.GONE);
        }
    }

    private void showNoData() {
        if (mZeroView != null) {
            mZeroView.setText(R.string.no_data);
            show(mZeroView);
        }
    }

    private void showError() {
        if (mZeroView != null) {
            mZeroView.setText(R.string.error);
            show(mZeroView);
        }
    }

    private void onSuccess(Weather weather) {
        mHasError = false;

        final List<Forecast> items = (weather != null) ? weather.getForecasts() : null;

        mAdapter.setItems(items);

        if (isEmpty(items)) {
            showNoData();
        } else if (mZeroView != null) {
            hide(mZeroView);
        }
    }

    private void onError() {
        mHasError = true;
        showError();
    }

    void onFinish() {
        mWeatherCall = null;
        setLoading(false);
    }

    private void show(View view) {
        view.setVisibility(View.VISIBLE);
    }

    private void hide(View view) {
        view.setVisibility(View.GONE);
    }

    private Callback<Weather> mWeatherCallback = new Callback<Weather>() {
        @Override
        public void onResponse(Response<Weather> response) {

            if (response.isSuccess()) {
                onSuccess(response.body());
            } else {
                onError();
            }

            onFinish();
        }

        @Override
        public void onFailure(Throwable t) {
            onError();
            onFinish();
        }
    };

    private ForecastAdapter.Callback mAdapterCallback = new ForecastAdapter.Callback() {

        @Override
        public void onItemClick(int position) {
            Context context = getContext();
            if (context != null) {
                context.startActivity(
                        ForecastActivity.newIntent(context, mAdapter.getItem(position))
                );
            }
        }

    };

}
