package org.example.weather.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import org.example.weather.App;
import org.example.weather.di.component.AppComponent;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
public class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        inject(getApp().component());
    }

    protected App getApp() {
        return (App) getActivity().getApplication();
    }

    protected void inject(AppComponent component) {
    }

}
