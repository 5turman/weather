package org.example.weather.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import org.example.weather.R;
import org.example.weather.api.dto.Forecast;
import org.example.weather.api.dto.Values;
import org.example.weather.api.dto.Wind;
import org.example.weather.ui.view.ValueView;
import org.example.weather.util.Views;

import static org.example.weather.util.Formatter.formatDateTime;
import static org.example.weather.util.Formatter.formatTemp;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
public class ForecastActivity extends AppCompatActivity {

    private static final String KEY_FORECAST = "forecast";

    public static Intent newIntent(Context context, Forecast forecast) {
        Intent intent = new Intent(context, ForecastActivity.class);
        intent.putExtra(KEY_FORECAST, forecast);
        return intent;
    }

    private Forecast mForecast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mForecast = getIntent().getParcelableExtra(KEY_FORECAST);

        setTitle(formatDateTime(mForecast));

        setContentView(R.layout.activity_content);

        initToolbar();
        addContent();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initToolbar() {
        Toolbar toolbar = Views.find(this, R.id.toolbar);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void addContent() {
        FrameLayout container = Views.find(this, R.id.content);
        getLayoutInflater().inflate(R.layout.include_fragment_forecast, container, true);

        ValueView valueView = Views.find(this, R.id.temp);
        Values temp = mForecast.getTemperature();
        valueView.bind(formatTemp(temp.getMin()), formatTemp(temp.getMax()));

        valueView = Views.find(this, R.id.wind);
        Wind wind = mForecast.getWind();
        valueView.bind(String.valueOf(wind.getMin()), String.valueOf(wind.getMax()));
    }

}
