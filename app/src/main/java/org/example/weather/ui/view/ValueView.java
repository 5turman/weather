package org.example.weather.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.widget.TextView;

import org.example.weather.R;
import org.example.weather.util.Views;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
public class ValueView extends CardView {

    private TextView minText;
    private TextView maxText;

    public ValueView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ValueView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {

        inflate(context, R.layout.include_value_view, this);

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.ValueView);
        CharSequence title = array.getText(R.styleable.ValueView_title);
        array.recycle();

        TextView textView = Views.find(this, R.id.title);
        textView.setText(title);

        minText = Views.find(this, R.id.min_value);
        maxText = Views.find(this, R.id.max_value);
    }

    public void bind(String minValue, String maxValue) {
        minText.setText(minValue);
        maxText.setText(maxValue);
    }

}
