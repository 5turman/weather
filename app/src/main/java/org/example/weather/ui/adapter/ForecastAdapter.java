package org.example.weather.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.example.weather.R;
import org.example.weather.api.dto.Forecast;
import org.example.weather.api.dto.Values;
import org.example.weather.util.Views;

import java.util.Collections;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.example.weather.util.Formatter.formatDateTime;
import static org.example.weather.util.Formatter.formatTemp;

/**
 * Created by Andrey Babinov on 22.01.16.
 */
public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ViewHolder> {

    public interface Callback {
        void onItemClick(int position);
    }

    private final Callback callback;

    private List<Forecast> items = emptyList();

    public ForecastAdapter(Callback callback) {
        this.callback = callback;
    }

    public void setItems(List<Forecast> items) {
        this.items = (items != null) ? items : Collections.<Forecast>emptyList();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_forecast, parent, false);
        return new ViewHolder(view, callback);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public Forecast getItem(int position) {
        return items.get(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private final Callback callback;

        private final TextView dayTimeText;
        private final TextView minValueText;
        private final TextView maxValueText;

        public ViewHolder(View itemView, Callback callback) {
            super(itemView);
            this.callback = callback;

            dayTimeText = Views.find(itemView, R.id.date_time);
            minValueText = Views.find(itemView, R.id.min_value);
            maxValueText = Views.find(itemView, R.id.max_value);

            itemView.setOnClickListener(this);
        }

        public void bind(Forecast forecast) {
            dayTimeText.setText(formatDateTime(forecast));

            Values temp = forecast.getTemperature();

            minValueText.setText(formatTemp(temp.getMin()));
            maxValueText.setText(formatTemp(temp.getMax()));
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (callback != null && position != RecyclerView.NO_POSITION) {
                callback.onItemClick(position);
            }
        }

    }

}
